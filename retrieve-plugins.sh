#!/bin/bash
set -e
set -o pipefail

if [[ -z "$1" || -z "$2" ]]; then
 echo "usage: $0 <jenkins-admin-user> <jenkins-admin-user-password>"
 exit 1
fi

# jq must be installed
# jenkins must be installed and exposed under https://<jenkins-url>
# User with Password had been configured
curl -s -k "http://$1:$2@<jenkins-url>/pluginManager/api/json?depth=1"   | jq -r '.plugins[].shortName' | tee plugins.txt