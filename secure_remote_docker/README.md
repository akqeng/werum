# Creating TLS-Files to secure remote docker host (based on [official documentation](https://docs.docker.com/engine/security/https/))

```bash
# run bash script to generate files
./secureRemoteDocker.sh
```