#!/bin/bash
# setup-ansible-user
# create ansible user for Ansible automation
# and configuration management
# create ansible user
getentUser=$(/usr/bin/getent passwd ansible)
if [ -z "$getentUser" ]
then
  echo "User ansible does not exist.  Will Add..."
  /usr/sbin/groupadd -g 2002 ansible
  /usr/sbin/useradd -u 2002 -g 2002 -c "Ansible Automation Account" -s /bin/bash -m -d /home/ansible ansible
echo "ansible:thisI5VerySecret!" | /sbin/chpasswd
mkdir -p /home/ansible/.ssh
fi
# setup ssh authorization keys for Ansible access 
echo "setting up ssh authorization keys..."
cat << 'EOF' >> /home/ansible/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgnoamhbscZY8zT88k9MPBdMLhIpXxqk49+T0aKZTELdItXAUVVHC/Xn8+mh06xGxq7EL4ifa9FExVu0Psnd81cI8Jt3OcBqBoykDXZ/36is9z9kemz8xWkfS8jpOC7hlSe4n524ZT0vRrK/QZbYU0JyOB35+I/Q20tMaqGfB2e3cfM7N/qkEHJ7mIDC8T9fJq+UqJQHv/nfKUNYc9Ei9SXoRHwsC0a+dRkHS2IDNU73pUsDWp0oOnJKo6b62L4aS/xIxU6Q+8rqjdaQn9vNdpOfchWapWosPeGtm7zb9G37Q0t7qo5MMz8P8zdFzZq2g/liGZkfThTiY61okPNRUf
EOF
chown -R ansible:ansible /home/ansible/.ssh
chmod 700 /home/ansible/.ssh
# setup sudo access for Ansible
if [ ! -s /etc/sudoers.d/ansible ]
then
echo "User ansible sudoers does not exist.  Will Add..."
cat << 'EOF' > /etc/sudoers.d/ansible
User_Alias ANSIBLE_AUTOMATION = %ansible
ANSIBLE_AUTOMATION ALL=(ALL)      NOPASSWD: ALL
EOF
chmod 400 /etc/sudoers.d/ansible
fi
# disable login for ansible except through 
# ssh keys
cat << 'EOF' >> /etc/ssh/sshd_config
Match User ansible
        PasswordAuthentication no
        AuthenticationMethods publickey
EOF
# restart sshd
systemctl restart sshd
# end of script