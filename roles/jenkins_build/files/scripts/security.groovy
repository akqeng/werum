#!groovy
 
import jenkins.model.*
import hudson.security.*
import hudson.security.csrf.DefaultCrumbIssuer
import jenkins.security.s2m.AdminWhitelistRule
import hudson.markup.RawHtmlMarkupFormatter

Jenkins j = Jenkins.instance

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", "superdupersecure")
j.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
j.setAuthorizationStrategy(strategy)
j.save()
 
j.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

// disable CLI remoting
//j.getDescriptor("jenkins.CLI").get().setEnabled(false)

// enable csrf protection
j.setCrumbIssuer(new DefaultCrumbIssuer(true))
j.save()

// set markup formatter to html
j.markupFormatter = new RawHtmlMarkupFormatter(false)
j.save()

// set agent protocols to secure version 4
Set<String> agentProtocolsList = ['JNLP4-connect', 'Ping']
if(!j.getAgentProtocols().equals(agentProtocolsList)) {
    j.setAgentProtocols(agentProtocolsList)
    j.save()
}
