#!groovy

import jenkins.model.*
import hudson.security.*
import org.jenkinsci.plugins.*

String server = "ldap://<the-ldap-api-url-of-ad>:389"
String rootDN = "<ask-your-ldap-or-ad-responsible>"
String userSearchBase = ''
String userSearch = "<ask-your-ldap-or-ad-responsible>"
String groupSearchBase = ""
String managerDN = "<ask-your-ldap-or-ad-responsible>"
String managerPassword = "<ask-your-ldap-or-ad-responsible>"
boolean inhibitInferRootDN = <ask-your-ldap-or-ad-responsible>
    
SecurityRealm ldap_realm = new LDAPSecurityRealm(server, rootDN, userSearchBase, userSearch, groupSearchBase, managerDN, managerPassword, inhibitInferRootDN) 
Jenkins.instance.setSecurityRealm(ldap_realm)
Jenkins.instance.save()
